import time
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from apps.accounts.models import Account
from utils import setup_browser, setup_account, force_login


class EventsTest(StaticLiveServerTestCase):
    fixtures = ["core/fixtures/initial_data.json"]

    def setUp(self):
        self.browser = setup_browser()
        self.account = setup_account()
        force_login(self.account, self.browser, self.live_server_url)

    def tearDown(self):
        self.browser.quit()

    def test_create_page(self):
        # Go to Home Directory
        self.browser.get(self.live_server_url + "/")

        # Go to Events Creation
        self.browser.get(self.live_server_url + "/events/create/")

        # Filling out the Form to create a Website
        self.browser.find_element_by_name("title").send_keys("Test")
        self.browser.find_element_by_name("description").send_keys("This is a Test")
        self.browser.find_element_by_name("start_date").send_keys("2020-03-01 11:45")
        self.browser.find_element_by_name("end_date").send_keys("2020-03-01 13:45")
        self.browser.find_element_by_name("location").send_keys("1234 Washington Blvd")
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        self.browser.find_element_by_xpath('//button[text()="Submit"]').click()
        time.sleep(0.5)
        assert "The event has been successfully created." in self.browser.find_element_by_class_name("alert-dismissible").text

        # Go to Calender
        self.browser.get(self.live_server_url + "/events/?month=2020-3")

        # Check to See if Event was created
        assert "Test" in self.browser.page_source

        # Go to Edit Page and Edit Date
        self.browser.get(self.live_server_url + "/events/1/edit/")
        self.browser.find_element_by_name("title").send_keys("Title")
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        self.browser.find_element_by_xpath('//button[text()="Submit"]').click()

        # Check April for Test
        self.browser.get(self.live_server_url + "/events/?month=2020-3")
        assert "Title" in self.browser.page_source

        # View test URL
        self.browser.get(self.live_server_url + "/events/1")
        assert "Title" in self.browser.page_source

        # Delete Event
        self.browser.get(self.live_server_url + "/events/1/delete/")
        self.browser.find_element_by_xpath('//button[text()="Delete"]').click()
        time.sleep(0.5)
        assert "The event has been deleted." in self.browser.find_element_by_class_name("alert").text

    def test_max_participants(self):
        # Go to Events Creation
        self.browser.get(self.live_server_url + "/events/create/")

        # Filling out the Form to create a Website
        self.browser.find_element_by_name("title").send_keys("TestParticipants")
        self.browser.find_element_by_name("description").send_keys("This is a Test")
        self.browser.find_element_by_name("start_date").send_keys("2020-03-01 11:45")
        self.browser.find_element_by_name("end_date").send_keys("2020-03-01 13:45")
        self.browser.find_element_by_name("location").send_keys("1234 Washington Blvd")
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        self.browser.find_element_by_xpath('//button[text()="Submit"]').click()

        # Check alert that says event was created succesfully.
        time.sleep(0.5)
        assert "The event has been successfully created." in self.browser.find_element_by_class_name("alert").text

        # Test to make sure the event was created
        self.browser.get(self.live_server_url + "/events/2")
        assert "TestParticipants" in self.browser.page_source

        # Force Login
        self.client.force_login(Account.objects.create_user(email="test@test.com", password="test", first_name="Name"))
        self.browser.get(self.live_server_url + "/events/2/check/")
        # Check into Event
        time.sleep(0.5)
        assert (
            "You can only check in 24 hours before and after an event."
            in self.browser.find_element_by_class_name("alert").text
        )
