from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.shortcuts import render
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import Group
from django.template.loader import render_to_string
from django.contrib.auth.tokens import default_token_generator as token_generator
from django.utils.encoding import force_bytes, force_text
from django.utils.translation import gettext_lazy as _
from django.utils.encoding import force_text
from django.utils.html import strip_tags
from django.core.mail import send_mail
from django.utils.http import urlsafe_base64_decode
from django.views import View
from django.views.generic import ListView as GenericListView
from django.views.generic.base import ContextMixin

from core.mixins import AccessRestrictedMixin, AccessModelMixin, NextPageMixin
from apps.accounts.forms import *
from core.views import LibreDeleteView, LibreDetailView, LibreUpdateView, LibreCreateView


class ListView(AccessRestrictedMixin, GenericListView):
    permissions = ("accounts.list_accounts", "accounts.edit_accounts", "accounts.delete_accounts")
    model = Account


class DetailView(AccessRestrictedMixin, LibreDetailView):
    personal = True
    permissions = ("accounts.view_accounts", "accounts.edit_accounts", "accounts.delete_accounts")
    model = Account
    pk_url_kwarg = "uid"


class CreateView(AccessRestrictedMixin, LibreCreateView):
    superuser = True
    template_name = "accounts/account_create.html"
    form_class = AccountForm
    success_url = "/accounts/"
    success_message = _("The account has been successfully created.")

    def post(self, request):
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.is_superuser = False
            account.save()

            url = request.build_absolute_uri("/accounts/password/reset/")
            url += urlsafe_base64_encode(force_bytes(account.uid)) + "/"
            url += token_generator.make_token(account) + "/"

            html = render_to_string(
                "email.html",
                {
                    "url": url,
                    "message": f"{account.first_name}, activate your new account using the link below.",
                    "button": "Activate Account",
                },
            )
            text = strip_tags(html).replace("Activate Account", url)

            send_mail(
                subject="Activate Account | LibreOrganize",
                message=text,
                html_message=html,
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=(account.email,),
                fail_silently=True,
            )
            messages.success(self.request, self.success_message)
            return HttpResponseRedirect(reverse("accounts:list"))
        return render(request=request, template_name="accounts/account_create.html", context={"form": form})


class EditView(AccessRestrictedMixin, LibreUpdateView):
    personal = True
    permissions =("accounts.edit_accounts",)
    model = Account
    form_class = EditForm
    pk_url_kwarg = "uid"
    success_url = reverse_lazy("accounts:list")
    success_message = _("The account has been successfully edited.")


class DeleteView(AccessRestrictedMixin, LibreDeleteView):
    permissions = ("accounts.delete_accounts",)
    model = Account
    pk_url_kwarg = "uid"
    success_url = reverse_lazy("accounts:list")
    success_message = _("The account has been successfully deleted.")


class LoginView(View):
    def dispatch(self, request):
        self.next = request.GET.get("next", None)
        if not self.next or "://" in self.next or " " in self.next:
            self.next = "/"
        if request.user.is_authenticated:
            messages.add_message(request, messages.WARNING, _("You are already logged in!"))
            return HttpResponseRedirect(self.next)
        return super().dispatch(request)

    def get(self, request):
        form = LoginForm()
        return render(request=request, template_name="accounts/login.html", context={"form": form})

    def post(self, request):
        form = LoginForm(request.POST)
        if form.login(request):
            messages.add_message(
                request,
                messages.SUCCESS,
                _(f"Welcome back, {request.user.first_name}! You have successfully logged in."),
            )
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="accounts/login.html", context={"form": form})


class LogoutView(View):
    def get(self, request):
        if request.user.is_authenticated:
            logout(request)
            messages.add_message(request, messages.SUCCESS, _("You have successfully logged out."))
        else:
            messages.add_message(request, messages.WARNING, _("You are already logged out!"))
        return HttpResponseRedirect(reverse("home"))


class RegisterView(NextPageMixin, ContextMixin, View):
    def dispatch(self, request):
        if request.user.is_authenticated:
            messages.add_message(request, messages.WARNING, _("You cannot register an account while being logged in!"))
            return HttpResponseRedirect(reverse("home"))
        return super().dispatch(request)

    def get(self, request):
        form = RegistrationForm()
        return render(request=request, template_name="accounts/register.html", context=self.get_context_data(form=form))

    def post(self, request):
        form = RegistrationForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.add_message(
                request,
                messages.SUCCESS,
                _(
                    f"Welcome to {settings.SITE_NAME}, {form.cleaned_data['first_name']}! You have successfully registered."
                ),
            )
            return HttpResponseRedirect(reverse("accounts:login"))
        return render(request=request, template_name="accounts/register.html", context=self.get_context_data(form=form))


class PasswordResetView(View):
    def dispatch(self, request):
        if request.user.is_authenticated:
            messages.add_message(
                request,
                messages.WARNING,
                _("You have been redirected to change your password because you are logged in!"),
            )
            return HttpResponseRedirect(reverse("accounts:password_change", args={request.user.uid}))
        return super().dispatch(request)

    def get(self, request):
        form = PasswordResetForm()
        return render(request=request, template_name="accounts/password/reset.html", context={"form": form})

    def post(self, request):
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            form.save(request=request)
            messages.add_message(
                request, messages.SUCCESS, _("You have successfully requested a password reset."),
            )
            return HttpResponseRedirect(reverse("accounts:login"))
        return render(request=request, template_name="accounts/password/reset.html", context={"form": form})


class PasswordResetConfirmView(NextPageMixin, ContextMixin, View):
    def dispatch(self, request, uidb64, token):
        if request.user.is_authenticated:
            messages.add_message(
                request,
                messages.WARNING,
                _("You have been redirected to change your password because you are logged in!"),
            )
            return HttpResponseRedirect(reverse("accounts:password_change", args={request.user.uid}))
        try:
            self.account = Account.objects.get(uid=force_text(urlsafe_base64_decode(uidb64)))
            if token_generator.check_token(self.account, token):
                return super().dispatch(request)
            raise Account.DoesNotExist
        except (TypeError, ValueError, OverflowError, Account.DoesNotExist):
            messages.add_message(request, messages.ERROR, _("The request is invalid."))
            return HttpResponseRedirect(reverse("accounts:password_reset"))

    def get(self, request):
        form = PasswordResetConfirmForm()
        return render(
            request=request, template_name="accounts/password/reset.html", context=self.get_context_data(form=form)
        )

    def post(self, request):
        form = PasswordResetConfirmForm(request.POST)
        if form.is_valid():
            form.save(account=self.account)
            messages.add_message(request, messages.SUCCESS, _("You have successfully reset your password."))
            return HttpResponseRedirect(reverse("accounts:login"))
        return render(
            request=request, template_name="accounts/password/reset.html", context=self.get_context_data(form=form)
        )


class PasswordChangeView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, ContextMixin, View):
    permissions = ("accounts.change_passwords",)
    personal = True
    model = Account

    def get(self, request):
        form = PasswordChangeForm()
        return render(
            request=request, template_name="accounts/password/change.html", context=self.get_context_data(form=form)
        )

    def post(self, request):
        form = PasswordChangeForm(request.POST)
        if form.is_valid():
            form.save(self.account)
            messages.add_message(request, messages.SUCCESS, _("You have successfully changed the password."))
            return HttpResponseRedirect(self.next)
        return render(
            request=request, template_name="accounts/password/change.html", context=self.get_context_data(form=form)
        )


class EditPermissionsView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, ContextMixin, View):
    permissions = ("accounts.delete_accounts",)
    model = Account

    def get(self, request):
        permissions = [perm.id for perm in Permission.objects.filter(user=self.account)]
        form = EditPermissionsForm(initial={"superuser": self.account.is_superuser, "permissions": permissions})
        return render(
            request=request, template_name="accounts/permissions/edit.html", context=self.get_context_data(form=form)
        )

    def post(self, request):
        form = EditPermissionsForm(request.POST)
        if form.is_valid():
            form.save(self.account)
            messages.add_message(request, messages.SUCCESS, _("You have successfully changed the permissions."))
            return HttpResponseRedirect(self.next)
        return render(
            request=request, template_name="accounts/permissions/edit.html", context=self.get_context_data(form=form)
        )


class AttendanceView(AccessRestrictedMixin, AccessModelMixin, View):
    permissions = ("accounts.view_accounts", "events.list_events")
    personal = True
    model = Account

    def get(self, request):
        return render(request=request, template_name="accounts/attendance/list.html", context={"account": self.account})


class GroupsListView(AccessRestrictedMixin, GenericListView):
    permissions = ("accounts.view_accounts",)
    model = Group
    template_name = "accounts/groups/list.html"


class GroupsCreateView(AccessRestrictedMixin, NextPageMixin, LibreCreateView):
    superuser = True
    template_name = "accounts/groups/create.html"
    form_class = GroupForm
    success_url = "/accounts/groups/"
    success_message = _("The group has been successfully created.")


class GroupsDetailView(AccessRestrictedMixin, LibreDetailView):
    permissions = ("accounts.view_accounts", "accounts.edit_accounts", "accounts.delete_accounts")
    model = Group
    pk_url_kwarg = "uid"
    template_name = "accounts/groups/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context["permissions"] = set(
            Permission.objects.exclude(
                content_type__app_label__in=["auth", "sessions", "contenttypes", "social_django"]
            )
        )
        group_perms = set(context["group"].permissions.all())
        for permission in context["permissions"]:
            if permission in group_perms:
                permission.have = True
            else:
                permission.have = False
        return context


class GroupsEditView(AccessRestrictedMixin, NextPageMixin, ContextMixin, View):
    permissions = ("accounts.edit_accounts",)

    def dispatch(self, request, uid, *args, **kwargs):
        try:
            self.group = Group.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, self.model.DoesNotExist):
            messages.add_message(request, messages.ERROR, _(f"The group doesn't exist."))
            return HttpResponseRedirect("/")
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        permissions = [perm.id for perm in Permission.objects.filter(group=self.group)]
        form = EditGroupPermissionsForm(initial={"permissions": permissions})
        return render(
            request=request, template_name="accounts/permissions/edit.html", context=self.get_context_data(form=form)
        )

    def post(self, request):
        form = EditGroupPermissionsForm(request.POST)
        if form.is_valid():
            form.save(self.group)
            messages.add_message(request, messages.SUCCESS, _("You have successfully changed the permissions."))
            return HttpResponseRedirect(self.next)
        return render(
            request=request, template_name="accounts/permissions/edit.html", context=self.get_context_data(form=form)
        )


class GroupsDeleteView(AccessRestrictedMixin, NextPageMixin, LibreDeleteView):
    permissions = ("accounts.delete_accounts",)
    model = Group
    pk_url_kwarg = "uid"
    success_url = reverse_lazy("groups:list")
    success_message = _("The group has been successfully deleted.")
    template_name = "accounts/groups/delete.html"

class GroupsAddView(AccessRestrictedMixin, NextPageMixin, LibreUpdateView):
    superuser = True
    model = Account
    form_class = GroupAddForm
    pk_url_kwarg = "uid"
    success_url = reverse_lazy("groups:list")
    success_message = _("The user has been assigned to a group.")
    template_name = "accounts/groups/add.html"