��          �   %   �      p     q     x  0        �     �     �     �     �     �     �                    >     O  
   ^     i      p  -   �  ,   �  ;   �  9   (     b          �     �     �     �     �     �  5   �     "     3     <     P     b     n     z     �     �  %   �     �     �     �     �        (   #  +   L  /   x  7   �     �     �  
                  
                  	                                                                                                    Active Amount Are you sure you want to delete this membership? Create Membership Delete Delete Membership Edit Membership End Date Enroll Expiration Date Member Memberships Must be formatted as YYYY-MM-DD New Member | %s  Payment Method Start Date Submit The membership has been deleted. The membership has been successfully created. The membership has been successfully edited. There is already a membership associated with this account. There will be no more charges related to this membership. This action is irreversible. Type View Account View Membership You are already a member! Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-06 23:58-0400
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Activo Cantidad ¿Está seguro de que desea eliminar esta membresía? Crear Membresía Eliminar Eliminar Membresía Editar Membresía Fecha Final Inscribirse Fecha de Caducidad Miembro Membresías Debe estar formateada como AAAA-MM-DD Nuevo miembro | %s  Método de pago Fecha de inicio Enviar La membresía ha sido eliminada. La membresía se ha creado exitosamente. La membresía ha sido editada exitosamente. Ya hay una membresía asociada con esta cuenta. No habrá más cargos relacionados con esta membresía. Esta acción es irreversible. Tipo Ver Cuenta Ver Membresía Ya eres miembro! 