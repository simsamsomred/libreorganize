��          �      ,      �  f   �     �               '     6     ;  
   Q  	   \     f     o       (   �  '   �  -   �       B  "  }   e     �     �          #     5     <     W     g     w     ~     �  5   �  3   �  $        '     
                                       	                                  
                        Are you sure you want to delete %(full_name)s Email?
                         Create Newsletter Email Delete Delete Newsletter Email Delete Sign Up Edit Edit Newsletter Email Entry Time Full Name Language Newsletter List Submit The email has been successfully deleted. The email has been successfully edited. The newsletter has been successfully created. This action is irreversible. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                       ¿Estás seguro de que quieres eliminar %(full_name)s ¿Correo electrónico?
                         Crear Correo Electrónico Eliminar Eliminar Correo Electrónico Eliminar Registro Editar Editar Correo Electrónico Hora de Entrada Nombre Completo Idioma Lista de Boletines Enviar El correo electrónico se ha eliminado correctamente. El correo electrónico se ha editado correctamente. El boletín se ha creado con éxito. Esta acción es irreversible. 