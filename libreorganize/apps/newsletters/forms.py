import re
from django import forms
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from apps.newsletters.models import SignUp


class SignUpForm(forms.ModelForm):
    email = forms.EmailField(label=_("Email"))

    class Meta:
        model = SignUp
        fields = ["full_name", "email", "language"]
        labels = {"full_name": "Full Name", "language": "Language"}

    def clean_email(self):
        return self.cleaned_data["email"].lower()

    def clean_full_name(self):
        if not re.match(settings.NAME_REGEX, self.cleaned_data["full_name"]):
            raise forms.ValidationError("Enter a valid full name.")
        return self.cleaned_data["full_name"]
