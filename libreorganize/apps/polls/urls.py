from apps.polls import views
from django.urls import path
from django.views.generic import RedirectView

app_name = "apps.polls"

urlpatterns = [
    path("", views.PollListView.as_view(), name="list"),
    path("list/", views.PollPublicListView.as_view(), name="public_list"),
    path("create/", views.PollCreateView.as_view(), name="create"),
    path("<int:poll_id>/vote/", views.PollVoteView.as_view(), name="vote"),
    path("<int:poll_id>/voterank/", views.PollVoteRankedView.as_view(), name="voterank"),
    path("<int:poll_id>/results/", views.PollResultView.as_view(), name="results"),
    path("<int:uid>/", views.PollDetailView.as_view(), name="detail"),
    path("<int:uid>/edit/", views.PollEditView.as_view(), name="edit"),
    path("<int:uid>/delete/", views.PollDeleteView.as_view(), name="delete"),
]
