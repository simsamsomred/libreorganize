from django import forms
from django.conf import settings
from django.forms import BaseInlineFormSet, SelectMultiple
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

from tempus_dominus.widgets import DateTimePicker

from apps.polls.models import Poll, PollChoice, PollDecision


class PollForm(forms.ModelForm):
    class Meta:
        model = Poll
        fields = ["topic", "description", "poll_type", "start_date", "end_date"]
        labels = {
            "topic": "Topic",
            "description": "Description",
            "poll_type": "Type",
            "start_date": "Start Date",
            "end_date": "End Date",
        }
        widgets = {
            "start_date": DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar-alt fa-fw",
                        "up": "fas fa-chevron-up fa-fw",
                        "down": "fas fa-chevron-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
            "end_date": DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar-alt fa-fw",
                        "up": "fas fa-chevron-up fa-fw",
                        "down": "fas fa-chevron-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
        }

    def clean(self):
        self.cleaned_data = super().clean()
        if self._errors:
            return
        if self.cleaned_data["start_date"] > self.cleaned_data["end_date"]:
            raise forms.ValidationError(_("The start date must be less than the end date."))
        return self.cleaned_data


class PollChoicesFormSet(BaseInlineFormSet):
    def clean(self):
        choices = []
        duplicates = False
        count = 0

        for form in self.forms:
            if form.cleaned_data:
                # On 'create', we don't have DELETE elements.  If we do have a 'DELETE' element, we skip it anyway
                if "DELETE" not in form.cleaned_data.keys() or not form.cleaned_data["DELETE"]:
                    choice = form.cleaned_data["name"]
                    count += 1
                    if choice in choices:
                        duplicates = True
                    else:
                        choices.append(choice)

        if count < 2:
            raise forms.ValidationError(_("Polls must have at least two choices."))

        if duplicates:
            raise forms.ValidationError(_("Polls must have unique choices."))


class PollDecisionForm(forms.Form):
    choice_uid = forms.IntegerField(required=True)


class PollChoiceModelForm(forms.ModelForm):
    class Meta:
        model = PollChoice
        fields = ["name"]
        labels = {"name": "Answer"}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # For some reason, using this as the widget in Meta doesn't work, so applying it here
        self.fields["name"].widget.attrs["class"] = "textinput textInput form-control"
        self.fields["name"].widget.attrs["placeholder"] = "Enter Candidate/Choice/Answer Here"


class PollRankedDecisionForm(forms.Form):
    pass
