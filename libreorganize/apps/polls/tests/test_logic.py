from django.test import TestCase
import datetime
import time
from apps.accounts.models import Account
from apps.polls.models import Poll


class TestPollsListView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_closed_status(self):
        """Successful closed view"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "2020-05-14 2:25",
                "end_date": "2020-05-14 2:30",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Polls")
        response = self.client.get("/polls/", follow=True)
        self.assertContains(response, "Closed")

    def test_pending_status(self):
        """Successful pending view"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:30",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Polls")
        response = self.client.get("/polls/", follow=True)
        self.assertContains(response, "Pending")

    def test_active_status(self):
        """Successful active view"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Polls")
        response = self.client.get("/polls/", follow=True)
        self.assertContains(response, "Open")


class TestPollsCreateView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_get_view(self):
        """Successful get create view"""
        self.client.force_login(self.superuser)
        response = self.client.get("/polls/create/", follow=True,)
        self.assertContains(response, "Create Poll")

    def test_normal_create_view(self):
        """Successful normal create view"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "2020-05-14 2:25",
                "end_date": "2020-05-14 2:30",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Polls")

    def test_ranked_create_view(self):
        """Successful ranked create View"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Ranked",
                "description": "News",
                "poll_type": "ranked",
                "start_date": "2020-05-14 2:25",
                "end_date": "2020-05-14 2:30",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Polls")

    def test_invalid_perms(self):
        """Invalid perms"""
        self.client.force_login(self.user)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Ranked",
                "description": "News",
                "poll_type": "ranked",
                "start_date": "2020-05-14 2:25",
                "end_date": "2020-05-14 2:30",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "You don&#39;t have the required permissions.")

    def test_invalid_date(self):
        """Start date after end date"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Ranked",
                "description": "News",
                "poll_type": "ranked",
                "start_date": "2020-05-16 2:25",
                "end_date": "2020-05-14 2:30",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "The start date must be less than the end date.")

    def test_one_choice(self):
        """Test one choice"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Ranked",
                "description": "News",
                "poll_type": "ranked",
                "start_date": "2020-05-16 2:25",
                "end_date": "2020-05-14 2:30",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-TOTAL_FORMS": "1",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Polls must have at least two choices.")

    def test_unique_choice(self):
        """Test unique choices"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Ranked",
                "description": "News",
                "poll_type": "ranked",
                "start_date": "2020-05-16 2:25",
                "end_date": "2020-05-14 2:30",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 1",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Polls must have unique choices.")

    def test_invalid_edit(self):
        """invalid edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "",
                "description": "News",
                "poll_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Create Poll")


class TestPollsEditView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_successful_edit(self):
        """Successful edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/polls/1/edit/",
            {
                "topic": "Question Normal Edit",
                "description": "News",
                "poll_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "b",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "The poll has been edited.")

    def test_poll_active(self):
        """Poll is active so no editing"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "1999-05-14 2:25",
                "end_date": "1999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/polls/1/edit/", follow=True,)
        self.assertContains(response, "You cannot edit a poll after it has started.")

    def test_get_edit(self):
        """Successful get edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/polls/1/edit/", follow=True,)
        self.assertContains(response, "Edit Poll")
        response = self.client.get("/polls/1/edit/", follow=True,)
        self.assertContains(response, "Edit Poll")

    def test_invalid_edit(self):
        """invalid edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post(
            "/polls/1/edit/",
            {
                "topic": "",
                "description": "News",
                "poll_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "b",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.assertContains(response, "Edit Poll")


class TestPollsDeleteView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_successful_delete(self):
        """Test successful delete"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post("/polls/1/delete/", follow=True,)
        self.assertContains(response, "The poll has been deleted.")


class TestPollsPublicListView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_public_list(self):
        """Test public list"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/polls/list/", follow=True,)
        self.assertContains(response, "Question Normal")


class TestPollsVoteView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_template(self):
        """Check login template"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/polls/1/vote/", follow=True)
        self.assertTemplateUsed(response, template_name="polls/polldecision_normal.html")

    def test_get_vote(self):
        """Test get method vote"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/polls/1/vote/", follow=True,)
        self.assertContains(response, "Question Normal")

    def test_successful_vote(self):
        """Test get method vote"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post("/polls/1/vote/", {"choice_uid": 1}, follow=True,)
        self.assertRedirects(response, "/polls/list/")

    def test_already_voted(self):
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post("/polls/1/vote/", {"choice_uid": 1}, follow=True,)
        response = self.client.get("/polls/1/vote/", follow=True,)
        self.assertContains(response, "You have already voted!")
        self.assertRedirects(response, "/polls/list/")

    def test_poll_closed(self):
        """Test poll closed"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": "1999-05-14 2:25",
                "end_date": "1999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/polls/1/vote/", follow=True,)
        self.assertContains(response, "The poll is not open for voting.")

    def test_redirect_not_signedin(self):
        """redirect if user not signed in"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.client.logout()
        response = self.client.get("/polls/1/vote/", follow=True,)
        self.assertRedirects(response, "/accounts/login/")


class TestPollsVoteRankedView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_template(self):
        """Check login template"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "ranked",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/polls/1/vote/", follow=True)
        self.assertTemplateUsed(response, template_name="polls/polldecision_rank.html")

    def test_get_vote(self):
        """Test get method vote"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "ranked",
                "start_date": "9999-05-14 2:25",
                "end_date": "9999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/polls/1/voterank/", follow=True,)
        self.assertContains(response, "Question Normal")

    def test_poll_closed(self):
        """Test poll closed"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "ranked",
                "start_date": "1999-05-14 2:25",
                "end_date": "1999-05-14 2:26",
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/polls/1/voterank/", follow=True,)
        self.assertContains(response, "The poll is not open for voting.")

    def test_redirect_not_signedin(self):
        """redirect if user not signed in"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "ranked",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        self.client.logout()
        response = self.client.get("/polls/1/voterank/", follow=True,)
        self.assertRedirects(response, "/accounts/login/")

    def test_already_voted(self):
        """test already voted ranked"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=10)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "ranked",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post("/polls/1/voterank/", {"result-0": 1, "result-1": 2}, follow=True,)
        response = self.client.get("/polls/1/voterank/", follow=True,)
        self.assertRedirects(response, "/polls/list/")
        self.assertContains(response, "You have already voted!")


class TestPollsVoteResultsView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_results_normal(self):
        """Test results normal"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(seconds=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post("/polls/1/vote/", {"choice_uid": 1}, follow=True,)
        time.sleep(1)
        response = self.client.get("/polls/1/results/", follow=True,)
        self.assertContains(response, "choice 1")

    def test_results_ranked(self):
        """Test results ranked"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(seconds=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "ranked",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post("/polls/1/voterank/", {"result-0": 1, "result-1": 2}, follow=True,)
        time.sleep(1)
        response = self.client.get("/polls/1/results/", follow=True,)
        self.assertContains(response, "choice 1")

    def test_results_closed(self):
        """Make sure can't view results till poll closed"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(minutes=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "ranked",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.get("/polls/1/results/", follow=True,)
        self.assertContains(response, "The poll is not yet closed.")
        self.assertRedirects(response, "/polls/list/")

    def test_results_ranked_tied(self):
        """Test results ranked tied"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(seconds=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "ranked",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post("/polls/1/voterank/", {"result-0": 1, "result-1": 2}, follow=True,)
        self.client.force_login(self.user)
        response = self.client.post("/polls/1/voterank/", {"result-0": 2, "result-1": 1}, follow=True,)
        time.sleep(1)
        response = self.client.get("/polls/1/results/", follow=True,)
        self.assertContains(response, "Results")
        response = self.client.get("/polls/1/results/", follow=True,)
        self.assertContains(response, "Results")

    def test_results_normal_tied(self):
        """Test results normal tied"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(seconds=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        response = self.client.post("/polls/1/vote/", {"choice_uid": 1}, follow=True,)
        self.client.force_login(self.user)
        response = self.client.post("/polls/1/vote/", {"choice_uid": 2}, follow=True,)
        time.sleep(1)
        response = self.client.get("/polls/1/results/", follow=True,)
        self.assertContains(response, "Results")

    def test_no_votes(self):
        """Test no votes"""
        start = datetime.datetime.now()
        end = start + datetime.timedelta(seconds=1)
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/polls/create/",
            {
                "topic": "Question Normal",
                "description": "News",
                "poll_type": "normal",
                "start_date": start,
                "end_date": end,
                "pollchoice_set-0-uid": "",
                "pollchoice_set-0-name": "choice 1",
                "pollchoice_set-1-uid": "",
                "pollchoice_set-1-name": "choice 2",
                "pollchoice_set-TOTAL_FORMS": "3",
                "pollchoice_set-INITIAL_FORMS": "0",
                "pollchoice_set-MIN_NUM_FORMS": "0",
                "pollchoice_set-MAX_NUM_FORMS": "1000",
            },
            follow=True,
        )
        time.sleep(1)
        response = self.client.get("/polls/1/results/", follow=True,)
        self.assertContains(response, "Results")
