import datetime

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class Poll(models.Model):
    # This represents a vote to be taken
    uid = models.AutoField(primary_key=True)
    topic = models.CharField(max_length=128)
    description = models.TextField()
    poll_type = models.CharField(max_length=16, default="normal", choices=settings.TYPE_VOTING)
    start_date = models.DateTimeField(help_text=_("Must be formatted as YYYY-MM-DD HH:MM"))
    end_date = models.DateTimeField(help_text=_("Must be formatted as YYYY-MM-DD HH:MM"))
    winner = models.OneToOneField("PollChoice", null=True, blank=True, on_delete=models.CASCADE, related_name="+")

    class Meta:
        default_permissions = ()
        permissions = (
            ("create_polls", "Create polls"),
            ("edit_polls", "Edit polls"),
            ("delete_polls", "Delete polls"),
            ("list_polls", "List polls"),
        )

    def get_poll_status(self):
        now = datetime.datetime.now()
        if now >= self.end_date:
            return "closed"
        elif self.start_date <= now < self.end_date:
            return "open"
        return "pending"

    def get_poll_status_display(self):
        now = datetime.datetime.now()
        if self.start_date <= now < self.end_date:
            return _("Open")
        if now >= self.end_date:
            return _("Closed")
        return _("Pending")


class PollChoice(models.Model):
    # This represents an individual candidate or choice in a poll
    uid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)

    class Meta:
        default_permissions = ()
        ordering = ("name",)


class PollVote(models.Model):
    uid = models.AutoField(primary_key=True)

    class Meta:
        default_permissions = ()


class PollDecision(models.Model):
    # This represents a cast vote or elected choice made by an individual for PollChoice (e.g., candidate) in a Poll
    uid = models.AutoField(primary_key=True)
    vote = models.ForeignKey(PollVote, on_delete=models.CASCADE, default="", null=True, blank=True)
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    choice = models.ForeignKey(PollChoice, on_delete=models.CASCADE)
    time_cast = models.DateTimeField(auto_now_add=True)
    rank = models.IntegerField(null=True, blank=True)

    class Meta:
        default_permissions = ()
