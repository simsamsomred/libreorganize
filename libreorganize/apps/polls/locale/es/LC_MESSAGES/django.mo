��             +         �  *   �     �     �                 	   &     0     9  %   I     o     t     |     �  %   �     �     �     �  
   �     �     �               9  3   S     �     �     �  	   �     �     �  B  �  5        D     L     [     d     v     �     �     �  &   �     �  	   �     �  
     0     +   @     l  
   �     �     �     �     �     �     �  5   �     5     S     X     ]     j     p                                                                                
                                                  	              Are you sure you want to delete this poll? Closed Create Poll Delete Delete Poll Description Edit Poll End Date List of choices Must be formatted as YYYY-MM-DD HH:MM Open Pending Poll Results Polls Polls must have at least two choices. Polls must have unique choices. Public Polls Results Start Date Status Submit The poll has been created. The poll has been deleted. The poll has been edited. There will be no more charges related to this poll. This action is irreversible. Topic Type View Poll Vote Vote in Poll Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 ¿Estás seguro de que deseas eliminar esta encuesta? Cerrado Crear Encuesta Eliminar Eliminar Encuesta Descripción Editar Encuesta Fecha Final Lista de Opciones Debe tener el formato AAAA-MM-DD HH:MM Abierto Pendiente Resultados de la Encuesta Elecciones Las encuestas deben tener al menos dos opciones. Las encuestas deben tener opciones únicas. Encuestas Públicas Resultados Fecha de Inicio Estado Enviar Se ha creado la encuesta. La encuesta ha sido eliminada. La encuesta ha sido editada. No habrá más cargos relacionados con esta encuesta. Esta acción es irreversible. Tema Tipo Ver Encuesta Votar Votar en Encuesta 