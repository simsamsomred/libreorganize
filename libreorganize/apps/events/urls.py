from apps.events import views
from apps.events import utils
from django.urls import path

app_name = "apps.events"

urlpatterns = [
    path("", views.ListView.as_view(), name="list"),
    path("calendar/", views.CalendarView.as_view(), name="calendar"),
    path("archive/", views.ArchiveListView.as_view(), name="archive"),
    path("create/", views.CreateView.as_view(), name="create"),
    path("subscribe/", utils.EventFeed(), name="subscribe"),
    path("<int:uid>/", views.DetailView.as_view(), name="detail"),
    path("<int:uid>/edit/", views.EditView.as_view(), name="edit"),
    path("<int:uid>/delete/", views.DeleteView.as_view(), name="delete"),
    path("<int:uid>/check/", views.CheckView.as_view(), name="check"),
]
