import copy
from itertools import chain

from django.views import View
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, Http404
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

from core.mixins import AccessRestrictedMixin, AccessModelMixin
from apps.events.models import Event
from apps.events.forms import EventForm, CreateEventForm
from apps.events.utils import Calendar, get_date, prev_month, next_month
from core.views import LibreDeleteView, LibreDetailView, LibreUpdateView, LibreCreateView

class ArchiveListView(AccessRestrictedMixin, ListView):
    permissions = ("events.list_events",)
    model = Event
    template_name = 'events/event_list_archive.html'
    # Defaults the queryset to return events which have passed
    def get_queryset(self):
        return Event.objects.filter(start_date__lt = datetime.now())


class ListView(AccessRestrictedMixin, ListView):
    permissions = ("events.list_events",)
    model = Event
    # Defaults the queryset to return events which are upcoming
    def get_queryset(self):
        return Event.objects.filter(start_date__gte = datetime.now())


class DetailView(LibreDetailView):
    model = Event
    pk_url_kwarg = "uid"
    template_name = 'events/event_detail.html'
    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        if not self.request.user.is_authenticated and obj.is_private:
            raise Http404("No event matches the given query")
        return obj


class EditView(AccessRestrictedMixin, LibreUpdateView):
    permissions = ("events.edit_events",)
    form_class = EventForm
    model = Event
    pk_url_kwarg = "uid"
    success_url = reverse_lazy("events:list")
    success_message = _("The event has been successfully edited.")

    def form_valid(self, form):
        # Check if we are editing multiple events
        if "modalRadio" in self.request.POST and (
            self.request.POST["modalRadio"] == "2" or self.request.POST["modalRadio"] == "3"
        ):
            start_delta = form.cleaned_data["start_date"] - form.initial["start_date"]
            end_delta = form.cleaned_data["end_date"] - form.initial["end_date"]
            event = self.object

            # If updating all objects, start with the original object; otherwise start with the current object
            if self.request.POST["modalRadio"] == "3":
                while event.previous is not None:
                    event = event.previous

            # For every object, update the attributes and adjust the start & end times
            try:
                # Save the many-to-many fields and generic relations for this form.
                cleaned_data = form.cleaned_data
                exclude = form._meta.exclude
                fields = form._meta.fields
                opts = form.instance._meta
                    
                # don't set start_date and end_date
                if exclude == None:
                    exclude = ["start_date", "end_date"]
  
                while True:
                    for f in fields:
                        if not f in exclude and not f=="participants":
                            setattr(event, f, cleaned_data[f])

                    for f in chain(opts.many_to_many):
                        # if f.name in exclude:
                        #     continue
                        if f.name in cleaned_data:
                            f.save_form_data(event, cleaned_data[f.name])

                    if event.uid != self.object.uid:
                        event.start_date = event.start_date + start_delta
                        event.end_date = event.end_date + end_delta

                    event.save()  # save the object
                    event = event.subsequent  # now repeat for next object in list
            except Event.DoesNotExist:
                pass

            success_message = self.get_success_message(form.cleaned_data)
            if success_message:
                messages.success(self.request, success_message)
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super().form_valid(form)


class DeleteView(AccessRestrictedMixin, LibreDeleteView):
    permissions = ("events.delete_events",)
    model = Event
    pk_url_kwarg = "uid"
    success_url = reverse_lazy("events:list")
    success_message = _("The event has been deleted.")

    def delete(self, request, *args, **kwargs):
        if "modalRadio" in self.request.POST and self.request.POST["modalRadio"] == "1":
            event = self.get_object()
            try:
                subsequent = event.subsequent
                subsequent.previous = event.previous
                event.previous = None
                event.save()
                subsequent.save()
            except Event.DoesNotExist:
                pass

        return super().delete(request, *args, **kwargs)


class CreateView(AccessRestrictedMixin, LibreCreateView):
    permissions = ("events.create_events",)
    model = Event
    form_class = CreateEventForm
    success_url = reverse_lazy("events:list")
    success_message = _("The event has been successfully created.")

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        if self.request.GET.get("start_date", None):
            form.initial["start_date"] = datetime.fromtimestamp(int(self.request.GET["start_date"]))
        return form

    def form_valid(self, form):
        # Save the form results
        result = super().form_valid(form)

        if self.object.occurrences > 1:
            # Override form.save() to make copies if we have a recurring meeting
            event = self.object

            delta = relativedelta(weeks=1)
            if event.frequency == "monthly":
                delta = relativedelta(months=1)
            elif event.frequency == "yearly":
                delta = relativedelta(years=1)

            for occurrence in range(event.occurrences - 1):
                new_event = copy.copy(event)
                new_event.uid = None
                new_event.previous = event
                new_event.start_date = event.start_date + delta
                new_event.end_date = event.end_date + delta
                new_event.save(force_insert=True)
                event = new_event

        return result


class CalendarView(View):
    def get(self, request, *args, **kwargs):
        date = get_date(self.request.GET.get("month", None))
        calendar = Calendar(date.year, date.month)

        context = {
            "calendar": calendar.formatmonth(withyear=True, privateevents=request.user.is_authenticated),
            "prev_month": prev_month(date),
            "next_month": next_month(date),
            "date": date,
        }

        return render(request=request, template_name="events/calendar.html", context=context)


class CheckView(AccessRestrictedMixin, AccessModelMixin, View):
    model = Event

    def get(self, request):

        # Redirect if checking in is not enabled for this event
        if not self.event.can_check_in:
            messages.add_message(
                request, messages.ERROR, _("You cannot check in to this event.")
            )
            return HttpResponseRedirect(reverse("events:detail", args={self.event.uid}))

        event_start = self.event.start_date - timedelta(days=1)
        event_end = self.event.end_date + timedelta(days=1)
        if request.user in self.event.participants.all():
            messages.add_message(request, messages.WARNING, _("You have already checked in!"))
        else:
            if self.event.participants.count() >= self.event.max_participants and self.event.max_participants != -1:
                messages.add_message(request, messages.ERROR, _("There are no more empty seats for this event!"))
            elif event_start < datetime.now() < event_end:
                self.event.participants.add(request.user)
                messages.add_message(request, messages.SUCCESS, _("You checked in."))
            else:
                messages.add_message(
                    request, messages.ERROR, _("You can only check in 24 hours before and after an event.")
                )
        return HttpResponseRedirect(reverse("events:detail", args={self.event.uid}))