from django.core.management.base import BaseCommand, CommandError
from datetime import datetime
from django.conf import settings
from dateutil.relativedelta import relativedelta
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from apps.accounts.models import Account
from apps.events.models import Event

class Command(BaseCommand):
    help = "Sends email notifcations whenever there is an event"

    def handle(self, *args, **options):
        startdate = datetime.now()
        enddate = startdate + relativedelta(hours=1)
        for event in Event.objects.filter(start_date__range=[startdate, enddate]):
            if event.sent == False:
                for account in Account.objects.all():
                    if account.notification == True:
                        url = f"https://{settings.DOMAIN}/events/{event.uid}/"
                        html = render_to_string(
                            "email.html",
                            {
                                "url": url,
                                "message": f"{account.first_name}, You have an upcoming event",
                                "button": "View Event",
                            },
                        )
                        text = strip_tags(html).replace("View Event", url)

                        send_mail(
                            subject="Event | LibreOrganize",
                            message=text,
                            html_message=html,
                            from_email=settings.EMAIL_HOST_USER,
                            recipient_list=(account.email,),
                            fail_silently=True,
                        )
                        self.stdout.write(self.style.SUCCESS(f"Event notification sent!"))
                        event.sent = True
                        event.save()