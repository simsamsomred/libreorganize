��    "      ,  /   <      �  +   �     %     .  
   7     B     O     V     c  
   o     z  0   �     �  &   �     �      �  %     #   2     V     c  
   x     �     �  (   �  '   �  >   �  >   6  <   u  3   �  -   �          1     7     G     d  +   �  
   �  
   �  
   �     �     �     �     �            5        U  "   ]  
   �     �  ,   �  '   �     �     	     %	     5	     <	  *   Y	  ,   �	  A   �	  @   �	  @   4
  2   u
  0   �
     �
     �
     �
                        
                              	                 !                                                    "                                        Are you sure you want to delete this event? Calendar Check in Checked in Create Event Delete Delete Event Description Edit Event End Date Enter -1 for an unlimited number of participants Events Ignore if there is only one occurrence Location Must be a value between 1 and 12 Must be formatted as YYYY-MM-DD HH:MM Must be logged in to view the event Participants Should be an address Start Date Submit The event has been deleted. The event has been successfully created. The event has been successfully edited. The event must be shorter than 28 days if it reoccurs monthly. The event must be shorter than 365 days if it reoccurs yearly. The event must be shorter than 7 days if it reoccurs weekly. The number of occurrences must be between 1 and 12. There are no more empty seats for this event! This action is irreversible. Title You checked in. You have already checked in! Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-06 23:57-0400
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 ¿Seguro qué quieres eliminar este evento? Calendario Registrado Registrado Crear Evento Eliminar Eliminar Evento Descripción Editar Evento Fecha final Ingrese -1 para un número ilimitado de participantes Eventos Ignorar si solo hay una ocurrencia Ubicación Debe ser un valor entre 1 y 12 Debe estar formateado como AAAA-MM-DD HH: MM Debe iniciar sesión para ver el evento Participantes Debe ser una dirección Fecha de inicio Enviar El evento ha sido cancelado. El evento ha sido creado con exitosamente. El evento ha sido editado con éxitosamente. El evento debe durar menos de 28 días si se repite mensualmente. El evento debe durar menos de 365 días si se repite anualmente. El evento debe durar menos de 7 días si se repite semanalmente. El número de ocurrencias debe estar entre 1 y 12. ¡No hay más asientos vacíos para este evento! Esta acción es irreversible. Titulo Te registraste. ¡Ya te has registrado! 