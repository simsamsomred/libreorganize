from dateutil.relativedelta import relativedelta
from django.conf import settings
from django import forms
from django.utils.translation import gettext_lazy as _

from tempus_dominus.widgets import DateTimePicker

from apps.events.models import Event


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        help_texts = {
            "participants": "If a name is in blue they are checked into the event",
        }
        widgets = {
            "start_date": DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar-alt fa-fw",
                        "up": "fas fa-chevron-up fa-fw",
                        "down": "fas fa-chevron-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
            "end_date": DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar-alt fa-fw",
                        "up": "fas fa-chevron-up fa-fw",
                        "down": "fas fa-chevron-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
        }
        labels = {
            "start_date": "Start Date",
            "end_date": "End Date",
            "max_participants": "Max Participants",
            "is_private": "Private",
            "can_check_in": "Allow Checking In",
        }
        fields = (
            "title",
            "description",
            "start_date",
            "end_date",
            "location",
            "max_participants",
            "participants",
            "is_private",
            "can_check_in",
        )

    def clean(self):
        self.cleaned_data = super().clean()
        if self._errors:
            return
        if self.cleaned_data["start_date"] > self.cleaned_data["end_date"]:
            raise forms.ValidationError(_("The start date must be less than the end date."))
        return self.cleaned_data

    def clean_max_participants(self):
        if self.cleaned_data["max_participants"] < -1:
            raise forms.ValidationError(_("The maximum number of participants must be higher than -1."))
        return self.cleaned_data["max_participants"]


class CreateEventForm(forms.ModelForm):
    class Meta:
        model = Event
        exclude = ("participants",)
        widgets = {
            "start_date": DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar-alt fa-fw",
                        "up": "fas fa-chevron-up fa-fw",
                        "down": "fas fa-chevron-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
            "end_date": DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar-alt fa-fw",
                        "up": "fas fa-chevron-up fa-fw",
                        "down": "fas fa-chevron-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
        }
        labels = {
            "start_date": "Start Date",
            "end_date": "End Date",
            "max_participants": "Max Participants",
            "is_private": "Private",
            "can_check_in": "Allow Checking In",
        }
        fields = (
            "title",
            "description",
            "start_date",
            "end_date",
            "occurrences",
            "frequency",
            "location",
            "max_participants",
            "is_private",
            "can_check_in",
        )

    def clean(self):
        self.cleaned_data = super().clean()
        if self._errors:
            return
        if self.cleaned_data["start_date"] > self.cleaned_data["end_date"]:
            raise forms.ValidationError(_("The start date must be less than the end date."))
        if not self.instance.pk:
            if self.cleaned_data["occurrences"] == 1:
                return self.cleaned_data
            if self.cleaned_data["frequency"] == "weekly":
                if (self.cleaned_data["end_date"] - self.cleaned_data["start_date"]).total_seconds() > 604800:
                    raise forms.ValidationError(_("The event must be shorter than 7 days if it reoccurs weekly."))
            if self.cleaned_data["frequency"] == "monthly":
                if (self.cleaned_data["end_date"] - self.cleaned_data["start_date"]).total_seconds() > 2419200:
                    raise forms.ValidationError(_("The event must be shorter than 28 days if it reoccurs monthly."))
            if self.cleaned_data["frequency"] == "yearly":
                if (self.cleaned_data["end_date"] - self.cleaned_data["start_date"]).total_seconds() > 31536000:
                    raise forms.ValidationError(_("The event must be shorter than 365 days if it reoccurs yearly."))
        return self.cleaned_data

    def clean_occurrences(self):
        if self.cleaned_data["occurrences"] < 1 or self.cleaned_data["occurrences"] > 12:
            raise forms.ValidationError(_("The number of occurrences must be between 1 and 12."))
        return self.cleaned_data["occurrences"]

    def clean_max_participants(self):
        if self.cleaned_data["max_participants"] < -1:
            raise forms.ValidationError(_("The maximum number of participants must be higher than -1."))
        return self.cleaned_data["max_participants"]
