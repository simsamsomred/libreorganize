from django.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings

from apps.accounts.models import Account


class Event(models.Model):
    uid = models.AutoField(primary_key=True)
    title = models.CharField(max_length=48)
    description = models.TextField(blank=True)
    start_date = models.DateTimeField(help_text=_("Must be formatted as YYYY-MM-DD HH:MM"))
    end_date = models.DateTimeField(help_text=_("Must be formatted as YYYY-MM-DD HH:MM"))
    location = models.CharField(max_length=128, help_text=_("Should be an address"))
    max_participants = models.IntegerField(default=-1, help_text=_("Enter -1 for an unlimited number of participants"))
    participants = models.ManyToManyField(Account, blank=True)
    is_private = models.BooleanField(default=False, help_text=_("Must be logged in to view the event"))
    occurrences = models.IntegerField(default=1, help_text=_("Must be a value between 1 and 12"))
    frequency = models.CharField(
        choices=settings.FREQUENCY_CHOICES,
        help_text=_("Ignore if there is only one occurrence"),
        max_length=12,
        default="weekly",
    )
    previous = models.OneToOneField(
        "Event", on_delete=models.CASCADE, null=True, editable=False, related_name="subsequent"
    )
    sent = models.BooleanField(default=False)
    can_check_in = models.BooleanField(default=True, help_text="Allow atendees to check in to this event")

    class Meta:
        default_permissions = ()
        permissions = (
            ("list_events", "List events"),
            ("create_events", "Create events"),
            ("edit_events", "Edit events"),
            ("delete_events", "Delete events"),
        )
