from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView

from core.mixins import AccessRestrictedMixin

from apps.announcements.models import Announcement
from apps.announcements.forms import AnnouncementForm
from core.views import LibreCreateView, LibreUpdateView, LibreDetailView, LibreDeleteView


class ListView(ListView):
    model = Announcement
    context_object_name = 'announcements'
    ordering = ["-date"]


class CreateView(AccessRestrictedMixin, LibreCreateView):
    permissions = ("announcements.create_announcements",)
    form_class = AnnouncementForm
    model = Announcement
    success_url = reverse_lazy("announcements:list")
    success_message = _("The announcement has been successfully created.")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class DetailView(LibreDetailView):
    model = Announcement
    

class EditView(AccessRestrictedMixin, LibreUpdateView):
    permissions = ("announcements.edit_announcements",)
    form_class = AnnouncementForm
    model = Announcement
    success_url = reverse_lazy("announcements:list")
    success_message = _("The announcement has been successfully edited.")


class DeleteView(AccessRestrictedMixin, LibreDeleteView):
    permissions = ("announcements.delete_announcements",)
    model = Announcement
    success_url = reverse_lazy("announcements:list")
    success_message = _("The announcement has been successfully deleted.")