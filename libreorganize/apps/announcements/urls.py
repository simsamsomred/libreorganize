from apps.announcements import views
from django.urls import path

app_name = "apps.announcements"

urlpatterns = [
    path("", views.ListView.as_view(), name="list"),
    path("create/", views.CreateView.as_view(), name="create"),
    path("<slug:slug>/", views.DetailView.as_view(), name="detail"),
    path("<slug:slug>/edit/", views.EditView.as_view(), name="edit"),
    path("<slug:slug>/delete/", views.DeleteView.as_view(), name="delete"),
]
