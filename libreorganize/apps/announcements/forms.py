from django import forms
from django.utils.translation import gettext_lazy as _
from django_ckeditor_5.fields import CKEditor5Widget
from django.template.defaultfilters import slugify

from apps.announcements.models import Announcement


class AnnouncementForm(forms.ModelForm):
    class Meta:
        model = Announcement
        fields = ("title", "content")
        widgets = {"content": CKEditor5Widget(config_name="wiki")}

    def clean_title(self):
        try:
            announcement = Announcement.objects.get(slug=slugify(self.cleaned_data["title"]))
            if announcement == self.instance:
                raise Announcement.DoesNotExist
            raise forms.ValidationError(_("An announcement with a similar title already exists."))
        except Announcement.DoesNotExist:
            if self.cleaned_data["title"].lower() == "create":
                raise forms.ValidationError(_('"Create" is not a valid title.'))
            if len(self.cleaned_data["title"]) < 3:
                raise forms.ValidationError(_("The title must have at least 3 characters."))
            return self.cleaned_data["title"]
