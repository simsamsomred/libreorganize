from apps.announcements.models import Announcement


def announcements_processor(request):
    announcements = Announcement.objects.all().order_by("-date")[:1]
    return {"announcements":announcements}
