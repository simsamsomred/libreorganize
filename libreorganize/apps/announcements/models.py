from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify

from apps.accounts.models import Account


class Announcement(models.Model):
    uid = models.AutoField(primary_key=True)
    title = models.CharField(max_length=64)
    slug = models.SlugField(unique=True)
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True)

    class Meta:
        default_permissions = ()
        permissions = (
            ("create_announcements", "Create announcements"),
            ("edit_announcements", "Edit announcements"),
            ("delete_announcements", "Delete announcements"),
        )

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)
