from django.test import TestCase
from apps.accounts.models import Account
from apps.announcements.models import Announcement


class TestListView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and announcements"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.announcement1 = Announcement.objects.get(uid=1)
        self.announcement2 = Announcement.objects.get(uid=2)


class TestCreateView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and announcements"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_create_successful(self):
        """Test create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/",
            {
                "title": "This is a test",
                "slug": "this-is-a-test",
                "content": "abc abc testing",
                "date": "2020-03-01 11:45",
                "author": "William Walker",
            },
            follow=True,
        )
        self.assertContains(response, "The announcement has been successfully created.")

    def test_template(self):
        """Check create template"""
        self.client.force_login(self.superuser)
        response = self.client.post("/announcements/create/", follow=True)
        self.assertTemplateUsed(response, template_name="announcements/announcement_form.html")

    def test_invalid_url(self):
        """Test invalid URL"""
        self.client.force_login(self.superuser)
        response = self.client.get("/announcements/inexistent-announcement/edit/", follow=True)
        self.assertContains(response, "The announcement doesn&#39;t exist.")
        self.assertRedirects(response, "/")

        """More than three char in title"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/",
            {"title": "s", "slug": "s", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "The title must have at least 3 characters.")

        """Test invalid titles"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/",
            {"title": "create", "slug": "create", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "&quot;Create&quot; is not a valid title.")

    def test_taken_title(self):
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/",
            {"title": "Test", "slug": "test", "content": "This field is required."},
            follow=True,
        )
        self.assertContains(response, "An announcement with a similar title already exists.")

    def test_incorrect_title(self):
        """Check create unsuccessful (title)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/",
            {"title": "", "slug": "news", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "This field is required.")

    def test_incorrect_content(self):
        """Check create unsuccessful (content)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/", {"title": "News", "slug": "news", "content": ""}, follow=True,
        )
        self.assertContains(response, "This field is required.")


class TestDetailView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_announcement_detail_page(self):
        """Bring to Detail Page"""
        self.client.force_login(self.superuser)
        response = self.client.get("/announcements/test/", follow=True)
        self.assertContains(response, "title")


class TestEditView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_create(self):
        """Test create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/",
            {"title": "test", "slug": "test", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        response = self.client.post(
            "/announcements/test/edit/",
            {
                "title": "This is an edited test",
                "slug": "this-is-an-edited-test",
                "content": "abc abc testing",
                "date": "2020-03-01 11:45",
                "author": "William Walker",
            },
            follow=True,
        )
        self.assertContains(response, "The announcement has been successfully edited.")

    def test_incorrect_announcement(self):
        """Check edit unsuccessful (wiki)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/",
            {"title": "testing", "slug": "testing", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        response = self.client.get("/announcements/inexistent-announcement/edit/", follow=True)
        self.assertContains(response, "The announcement doesn&#39;t exist.")


class TestDeleteView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/announcements.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_edit(self):
        """Test delete"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/announcements/create/",
            {
                "title": "This is test",
                "slug": "this-is-a-test",
                "content": "abc abc testing",
                "date": "2020-03-01 11:45",
                "author": "William Walker",
            },
            follow=True,
        )
        self.assertContains(response, "The announcement has been successfully created.")
        response = self.client.post("/announcements/this-is-a-test/delete/?next=/announcements/", follow=True,)
        self.assertContains(response, "The announcement has been successfully deleted.")

    def test_incorrect_delete(self):
        """Check delete unsuccessful (wiki)"""
        self.client.force_login(self.superuser)
        response = self.client.get("/announcements/inexistent-wiki/delete/", follow=True)
        self.assertContains(response, "The announcement doesn&#39;t exist.")
