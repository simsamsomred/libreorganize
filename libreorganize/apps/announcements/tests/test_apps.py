from django.test import TestCase
from apps.announcements.apps import AnnouncementsConfig


class AnnouncementsAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(AnnouncementsConfig.name, "apps.announcements")