from django.test import TestCase
from django.urls import resolve


class AnnouncementsUrlsCase(TestCase):
    def test_list(self):
        """Check list URL correct"""
        self.assertEqual(resolve("/announcements/").view_name, "announcements:list")

    def test_create(self):
        """Check create URL correct"""
        self.assertEqual(resolve("/announcements/create/").view_name, "announcements:create")

    def test_detail(self):
        """Check detail correct"""
        self.assertEqual(resolve("/announcements/test/").view_name, "announcements:detail")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/announcements/test/edit/").view_name, "announcements:edit")

    def test_delete(self):
        """Check delete URL correct"""
        self.assertEqual(resolve("/announcements/test/delete/").view_name, "announcements:delete")