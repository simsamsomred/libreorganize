import re

from django.contrib import messages
from django.contrib.auth.views import redirect_to_login
from django.http import HttpResponseRedirect, Http404
from django.utils.translation import gettext_lazy as _


class AccessRestrictedMixin:
    permissions = None
    personal = False

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect_to_login(request.get_full_path(), "/accounts/login/", "next")
        if self.personal:
            if self.model.__name__.lower() == "account":
                if request.user.uid == kwargs.get("uid", -1):
                    return super().dispatch(request, *args, **kwargs)
            else:
                try:
                    model = getattr(request.user, self.model.__name__.lower())
                    if kwargs.get("uid", -1) == model.uid:
                        return super().dispatch(request, *args, **kwargs)
                except AttributeError:
                    model_set = getattr(request.user, self.model.__name__.lower() + "_set")
                    if model_set.filter(uid=kwargs.get("uid", -1)).exists():
                        return super().dispatch(request, *args, **kwargs)
                except self.model.DoesNotExist:
                    pass
        if not self.permissions:
            return super().dispatch(request, *args, **kwargs)
        if isinstance(self.permissions, str):
            print("Make sure permissions are a list not a string")
        else:
            for permission in self.permissions:
                if request.user.has_perm(permission):
                    return super().dispatch(request, *args, **kwargs)
        messages.add_message(request, messages.ERROR, _("You don't have the required permissions."))
        return HttpResponseRedirect("/")


class AccessModelMixin:
    model = None

    def dispatch(self, request, uid, *args, **kwargs):
        try:
            setattr(self, re.sub(r"(?<!^)(?=[A-Z])", "_", self.model.__name__).lower(), self.model.objects.get(uid=uid))
        except (TypeError, ValueError, OverflowError, self.model.DoesNotExist):
            messages.add_message(request, messages.ERROR, _(f"The {self.model.__name__.lower()} doesn't exist."))
            return HttpResponseRedirect("/")
        return super().dispatch(request, *args, **kwargs)


class Custom404Mixin:
    def dispatch(self, request, *args, **kwargs):
        try:
            return super().dispatch(request, args, kwargs)
        except Http404:
            messages.add_message(request, messages.ERROR, _(f"The {self.model.__name__.lower()} doesn't exist."))
            return HttpResponseRedirect("/")


class SuccessUrlMixin:
    def dispatch(self, request, *args, **kwargs):
        override_url = request.GET.get("next")
        if not override_url is None:
            setattr(self, "success_url", override_url)
        return super().dispatch(request, *args, **kwargs)


class NextPageMixin:
    success_url = "/"

    def dispatch(self, request, *args, **kwargs):
        setattr(self, "next", request.GET.get("next"))
        success_url = request.GET.get("next")
        if not getattr(self, "next") or "://" in getattr(self, "next") or " " in getattr(self, "next"):
            setattr(self, "next", "/")
            success_url = "/"
        setattr(self, "success_url", success_url)
        return super().dispatch(request, *args, **kwargs)
