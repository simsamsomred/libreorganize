from django.conf import settings

def version(request):
    kwargs = {
        'version': settings.VERSION_NUMBER,
    }
    return kwargs